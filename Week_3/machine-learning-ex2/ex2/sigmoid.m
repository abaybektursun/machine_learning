function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
g = zeros(size(z));
e = exp(1);

for colNum = 1 : size(z, 2)
    for rowNum = 1 : size(z, 1)
        g(rowNum, colNum) = 1/(1 + e^( -( z(rowNum, colNum) ) ));
    end
end

%

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the sigmoid of each value of z (z can be a matrix,
%               vector or scalar).





% =============================================================

end
