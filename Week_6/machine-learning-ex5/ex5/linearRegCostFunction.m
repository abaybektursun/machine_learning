function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% =========================================================================
n = size(theta);
h_t = h(X, theta);

J = (1/(2*m)) * ( (h_t - y)' * (h_t - y) ) + (lambda/(2*m))*(theta(2:n)' * theta(2:n));

grad = 1 / m * X' * (h_t - y);
grad(2:n) = grad(2:n) + (lambda/m) * theta(2:n);
% =========================================================================

grad = grad(:);

end
