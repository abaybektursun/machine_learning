function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

%--------------------------------------------------------------------------
%% Create length(y) by num_labels Y matrix by turning value y to num_labels dimensional class vector
Y = zeros(length(y), num_labels);
for yInd = 1 : length(y)
    temp_y = zeros(num_labels,1);
    temp_y(y(yInd)) = 1;
    Y(yInd,:) = temp_y;
end

%% Feedforward ---------------------------------------------------------

% Adding the bias unit
a1 = [ones(size(X,1),1) X];

% One step forward
z2 = Theta1*a1';
a2 = sigmoid(z2);

% Adding the bias unit to the hidden layer
a2_biased = [ones(1,size(a2,2)); a2];

% One more step forward
z3 = Theta2*a2_biased;
a3 = sigmoid(z3);
%----------------------------------------------------------------------

%% Cost------------------------------------------------------------------
for i = 1 : m
    J = J + (( -1*Y(i,:)*log(a3(:,i)) ) - ( (1-Y(i,:))*log(1-a3(:,i)) ) );
end
J = (1/m)*J;
% ----------------------------------------------------------------------

%% Regularization--------------------------------------------------------
squareSumTheta1 = 0;
for k = 2 : size(Theta1,2)
    for j = 1 : size(Theta1,1)
        squareSumTheta1 = squareSumTheta1 + Theta1(j,k)^2;
    end
end

squareSumTheta2 = 0;
for k = 2 : size(Theta2,2)
    for j = 1 : size(Theta2,1)
        squareSumTheta2 = squareSumTheta2 + Theta2(j,k)^2;
    end
end
J = J + (lambda/(2*m))*(squareSumTheta1 + squareSumTheta2);
%% Gradient----------------------------------------------------------------
    % Create a1 and Adding the bias unit
    a1 = [ones(size(X,1),1) X];
    % Create Deltas
    DELTA_1 = zeros(size(Theta1));
    DELTA_2 = zeros(size(Theta2));
    %size(DELTA_1)
    %size(DELTA_2)
for t = 1 : m
    % One step forward
    z2 = Theta1*a1(t,:)';
    a2 = sigmoid(z2);

    % Adding the bias unit to the hidden layer
    a2_biased = [ones(1,size(a2,2)); a2];

    % One more step forward
    z3 = Theta2*a2_biased;
    a3 = sigmoid(z3);

    %% Backprop------------------------------------------------------------
    % First Step Back (Calcualte error)
    delta_3 = (a3 - Y(t,:)');
    % Second Step Back (Calcualte error)
    % size(Theta2) = 10x26
    % size(delta_3) = 10x1
    % size(sigmoidGradient(z2)) = 25x1
    %                                             !BIAS ADDED HERE!
    delta_2 = (Theta2' * delta_3) .* sigmoidGradient([1; z2]);
    
    % grab dat grad (Modern Slang for Gradient Accumulation)
    %!BIAS WAS NOT REMOVED!
    DELTA_2 = DELTA_2 + delta_3*a2_biased';
    DELTA_1 = DELTA_1 + delta_2(2:end,:)*a1(t,:);
end

% Gradient for the neural network cost function
Theta1_grad(:,1)     = (1/m)*DELTA_1(:,1);
Theta1_grad(:,2:end) = (1/m)*DELTA_1(:,2:end)+(lambda/m)*Theta1(:,2:end);


Theta2_grad(:,1)     = (1/m)*DELTA_2(:,1);
Theta2_grad(:,2:end) = (1/m)*DELTA_2(:,2:end)+(lambda/m)*Theta2(:,2:end);

%size(DELTA_1)%25 401
%size(DELTA_2)%10 26

%% ========================================================================
% You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%
% -------------------------------------------------------------

% =========================================================================

%% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

end
