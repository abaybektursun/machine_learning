function J = computeCost(X, y, theta)
	%COMPUTECOST Compute cost for linear regression
	%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
	%   parameter for linear regression to fit the data points in X and y

	% Initialize some useful values
	m = length(y); % number of training examples

	% You need to return the following variables correctly 
	J = 0;

	thetaT = transpose(theta);
	XT = transpose(X);

	sum_t = thetaT*XT;
	sum_m = sum_t - transpose(y);
	sum_m = sum_m * transpose(sum_m);
	J = sum_m*(1/ (2*m) );

	% ====================== YOUR CODE HERE ======================
	% Instructions: Compute the cost of a particular choice of theta
	%               You should set J to the cost.





	% =========================================================================

end
